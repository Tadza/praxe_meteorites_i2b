/*
 *  Tadeas Bartosek, trsda I2B
 *  27.5.2017 16:54
 *  Praxe_01
 */

#include <stdio.h>

#include <stdlib.h>

#include <conio.h>

#include <malloc.h>

#include <math.h>

int main() {
	int meteo[6][50], i,k,test,pocet[6],max[6],min[6],*moreThanTen;
	for(k=0; k<6; k++) {
		for(i=0; i<50; i++) {
			meteo[k][i]=0;
		}
		max[k]=0;
		min[k]=50;
	}

	printf("zadave svetlo meteoritu\n");
	for(k=0; k<6; k++) {
		for(i=0; i<50; i++) {
			printf("meteotire %d hour %d \n",i+1,k+1);
			scanf("%d",&test);
			if(test==0) {
				i--;
				break;

			}
			if(test >20 || test<1) {
				i--;
				printf("light must be more than 1 and less than 20\n");

			}
			meteo[k][i]=test;
			if (meteo[k][i]>max[k])
				max[k]= meteo[k][i];
			if (meteo[k][i]<min[k])
				min[k]= meteo[k][i];
		}
		pocet[k]=i;
	}
	for(k=0; k<6; k++) {
		if (pocet[k]>0 && pocet[k]<=50) {
			printf("in hour %d number o meteorits %d\n",k+1,pocet[k]);
			printf("in hour %d Max light was %d\n",k+1,max[k]);
			printf("in hour %d min light was %d\n",k+1,min[k]);
		}
	}
	printf("printing out meteorites\n");
	test=0;
	for(k=0; k<6; k++) {
		printf("hour %d\n",k+1);
		for(i=0; i<50; i++) {
			printf("  %d  ",meteo[k][i]);
			if (meteo[k][i]==0)
			break;
			if (meteo[k][i]>10)
				test++;
		}

		printf("\n");
	}
	moreThanTen=(int*)malloc(sizeof(int)*test);
	test=0;
	for(k=0; k<6; k++) {
		for(i=0; i<50; i++) {
			if (meteo[k][i]>10) {
				moreThanTen[test]=meteo[k][i];
				test++;
			}
		}
	}
	for(i=0; i<test; i++) {
		printf("\n \t%d \n",moreThanTen[i]);
	}
free(moreThanTen);
moreThanTen=NULL;


	return 1;
}
